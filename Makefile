prefix = bot/
CC = python3

APP = bot.py

all:
clean:
install:
run:
	source venv/bin/activate && $(CC) $(prefix)$(APP)
uninstall:
