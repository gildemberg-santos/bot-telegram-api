# bot.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


# Modulos externos
import os
import re
import json
import asyncio
from termcolor import colored
from telethon import TelegramClient, events, utils, functions, types, Button
from telethon.tl.functions.channels import JoinChannelRequest, GetParticipantsRequest, InviteToChannelRequest
from telethon.tl.functions.messages import ImportChatInviteRequest
# Importando modulos locais
from configs import ConfigsTelegram, ConfigsProject
from src.cli.con import Con
from src.csv.listcsv import listCSV
from src.json.json import Json
from src.database.dao import Dao
# Variaveis Globais
global bot_token
global user


def config():
    try:
        os.mkdir("data")
        print("A pasta <data> foi criada com sucesso!")
    except Exception as e:
        if e.errno != 17:
            print(e)
            exit()
    _config = ConfigsTelegram()
    _json = Json()
    if not _config.data:
        print(
            "Tela de configuração esta em desenvolvimento. A configuração deve ser manual.")
    # Implementar aqui um metodo para alterar o arquivo de configuração
    url = "https://gitlab.com/-/snippets/2022406/raw/master/trial_bot_telegram_api.json"
    dw = _json.downloadJson(url=url)
    if not dw:
        exit()
    elif not dw[_config.get_number()]:
        print("Por favor verifique com o desenvolvedor se sua licença de uso do sistema esta valida!")
        exit()
    print(
        "Olá, sejá bem-vindo(a) --> {0}".format(_config.get_number()))
    print(
        colored('💽✅ Licença ativa!',
        'blue'))


config()


def start_bot():
    # Variavel Global
    _config = ConfigsTelegram()
    global bot_token
    global user
    print("1 - Entrar como Usuário \n2 - Entrar como Bot")
    opcao = str(input("Digite o número de sua opção: "))
    if opcao == "1":
        bot_token = None
    elif opcao == "2":
        bot_token = _config.get_bot_token()
    else:
        exit()
    try:
        pass
        # Remove a sessão para altenar entre usuario e bot
        # os.remove("{0}.session".format(_config.get_number())
    except BaseException:
        pass
    telegram = TelegramClient(
        _config.get_number(),
        _config.get_api_id(),
        _config.get_api_hash(),
        proxy=_config.get_poxy())
    if bot_token is None:
        user = telegram.start()
    else:
        user = telegram.start(bot_token=bot_token)
    user.parse_mode = 'html'


# Metodo responsavel por controlar o menu inicial
start_bot()

# RETORNA INFO DE CONECÇÃO


@user.on(events.NewMessage(pattern='/information', forwards=False))
async def information(event):
    """
    Metodo responsavel por mostrar se quem esta conectado (usuário e bot)
    """
    con = Con()
    con.clear()
    print(
        colored(
            con.titulo(),
            'blue'))
    warning = await event.reply(con.status(1))
    if await user.is_bot():
        await user.send_message(event._chat_peer.user_id, 'Você entrou como (Bot 🤖)!')
    else:
        await user.send_message(event._chat_peer.user_id, 'Você entrou como (Usuário 🤟🏻)!')
    await event.reply(con.status(2))
    print(colored('(Presione <Ctrl+C> para sair...)', 'red'))
    await event.delete()
    await warning.delete()


# ENCERRAR A CONECÇÃO


@user.on(events.NewMessage(pattern='/quit', forwards=False))
async def quit(event):
    """
    Metodo responsavel por encerrar a aplicação
    """
    con = Con()
    con.clear()
    print(
        colored(
            con.titulo(),
            'blue'))
    await event.reply('Conecção encerrada! \nExecute novamente o servidor para uma nova conecção')
    await user.disconnect()

# VERIFICA A CONECÇÃO


def verificarbot(def_bot=False):
    if not def_bot:
        if bot_token is None:
            return True
        else:
            return False
    else:
        if bot_token is not None:
            return True
        else:
            return False


# FUNÇÕES DO USUÁRIO
if verificarbot(False):
    @user.on(events.NewMessage(pattern='/scandialoggroups', forwards=False))
    async def scanDialogGroups(event):
        con = Con()
        con.clear()
        _configPro = ConfigsProject()
        print(
            colored(
                con.titulo(),
                'blue'))
        warning = await event.reply(con.status(1))
        global user
        csv = listCSV()
        listadechannel = []
        channel = csv.getList(_configPro.get_groups_csv_path())
        for item in channel:
            listadechannel.append(str(item[0]))
        async for dialog in user.iter_dialogs():
            try:
                if isinstance(dialog.entity, types.Channel) and dialog.entity.username and (
                        not(dialog.entity.username in listadechannel)):
                    msg = dialog.entity.username
                    csv.setList(
                        path=_configPro.get_groups_csv_path(),
                        _list=[
                            [msg]])
            except Exception as e:
                print(e)
        print(colored('(Presione <Ctrl+C> para sair...)', 'red'))
        await event.reply(con.status(2))
        await event.delete()
        await warning.delete()

    @user.on(events.NewMessage(pattern='/addgroup (.+)', forwards=False))
    async def addGroup(event):
        """
        Metodo responsavel por adicionar novos grupos para buscar participantes
        """
        global user
        con = Con()
        con.clear()
        _configPro = ConfigsProject()
        print(
            colored(
                con.titulo(),
                'blue'))
        warning = await event.reply(con.status(1))
        msg = event.message.message.replace("/addgroup", "").replace(" ", "")
        listCSV().setList(
            path=_configPro.get_groups_csv_path(),
            _list=[[msg]])
        print(colored('(Presione <Ctrl+C> para sair...)', 'red'))
        await event.reply(con.status(2))
        await event.delete()
        await warning.delete()

    @user.on(events.NewMessage(pattern='/scangroups', forwards=False))
    async def scanGroups(event):
        """
        Metodo responsavel por busca todos os participante de um grupo
        """
        global user
        con = Con()
        con.clear()
        _configPro = ConfigsProject()
        print(
            colored(
                con.titulo(),
                'blue'))
        warning = await event.reply(con.status(1))
        channels = listCSV().getList(_configPro.get_groups_csv_path())
        for count in range(len(channels)):
            try:
                await user(JoinChannelRequest(channels[count][0]))
            except Exception as e:
                print("try 01", e, channels[count][0])
                try:
                    await user(ImportChatInviteRequest(channels[count][0]))
                except Exception as e:
                    print("try 02", e, channels[count][0])
        print(colored('(Presione <Ctrl+C> para sair...)', 'red'))
        await event.reply(con.status(2))
        await event.delete()
        await warning.delete()

    @user.on(events.NewMessage(pattern='/listgroups', forwards=False))
    async def listGroups(event):
        """
        Metodo responsavel por lista todos os grupos salvos
        """
        global user
        con = Con()
        con.clear()
        print(
            colored(
                con.titulo(),
                'blue'))
        warning = await event.reply(con.status(1))
        _str = con.listChannels()
        print(_str)
        await user.send_message(event._chat_peer.user_id, _str)
        print(colored('(Presione <Ctrl+C> para sair...)', 'red'))
        await event.reply(con.status(2))
        await event.delete()
        await warning.delete()

    @user.on(events.NewMessage(pattern='/scanuser', forwards=False))
    async def scanUser(event):
        """
        Metodo responsavel por buscar todos os usuarios de um grupo
        """
        con = Con()
        con.clear()
        _configPro = ConfigsProject()
        dao = Dao()
        print(
            colored(
                con.titulo(),
                'blue'))
        warning = await event.reply(con.status(1))
        print(con.status(1))
        csv = listCSV()
        global user
        listadeuser = []
        # Pega os ids pelo csv
        channels = csv.getList(_configPro.get_groups_csv_path())
        # Pega os ids SQLite
        users = dao.getAllIdChatUser(values={})
        listadeuser.extend(users)
        del(users)
        for count in range(len(channels)):
            queryKey = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                        'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
            channelname = channels[count][0]
            for key in queryKey:
                offset = 0
                limit = 200
                while True:
                    try:
                        participants = await user(GetParticipantsRequest(channel=channelname, filter=types.ChannelParticipantsSearch(key), offset=offset, limit=limit, hash=0))
                    except BaseException:
                        break
                    if not participants.users:
                        break
                    for _user in participants.users:
                        try:
                            if re.findall(
                                    r"\b[a-zA-Z]", _user.first_name)[0].lower() == key:
                                for item in participants.users:
                                    if not(item.id in listadeuser):
                                        listadeuser.append(int(item.id))
                                        try:
                                            values = {"idUser":int(item.id), "username":str(item.username), "phone":str(item.phone)}
                                            dao.addUser(values=values)
                                        except Exception as e:
                                            print(e)
                        except Exception as e:
                            pass
                    offset += len(participants.users)
        _str = "Total {0}\n".format(len(listadeuser))
        _str = _str + con.status(2)
        print(_str)
        print(colored('(Presione <Ctrl+C> para sair...)', 'red'))
        await event.reply(_str)
        await event.delete()
        await warning.delete()

    @user.on(events.NewMessage(pattern='/getphone', forwards=False))
    async def getPhone(event):
        """
        Metodo responsavel por retornar uma lista de telefone coletados
        """
        con = Con()
        con.clear()
        print(
            colored(
                con.titulo(),
                'blue'))
        _str = con.listUserPhone()
        _str = _str + con.status(2)
        print(_str)
        print(colored('(Presione <Ctrl+C> para sair...)', 'red'))
        await event.reply(_str)


# FUNÇÕES DO BOT
if verificarbot(True):
    pass
try:
    con = Con()
    con.clear()
    print(
        colored(
            con.titulo(),
            'blue'))
    print(colored('(Presione <Ctrl+C> para sair...)', 'red'))
    user.run_until_disconnected()
finally:
    user.disconnect()

