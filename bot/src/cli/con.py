# interface.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


import os
from configs import ConfigsProject
from termcolor import colored
from src.csv.listcsv import listCSV


class Con:
    def __init__(self):
        """
        Metodo não tem nenhum ação
        """
        pass

    def clear(self):
        """
        Metodo responsavel de limpar tela do terminal
        """
        os.system("clear")

    def titulo(self):
        """
        Metodo responsavel pelo texto do titulo
        - retorna um str
        """
        _str = ""
        _str = _str + colored('Desenvolvido por: Gildemberg Santos --> 📱+5585991365507', 'blue')
        return _str

    def status(self, status : int):
        """
        Metodo responsavel pelo texto de status
        - retorna um str
        """
        _str = ""
        if status == 1:
            _str = _str + "🔄 Processing"
        elif status == 2:
            _str = _str + "✅ Process concluded"
        else:
            _str = _str + "⛔ Undefined Status"
        return _str

    def listChannels(self):
        """
        Metodo responsavel por lista todos os links das grupos
        """
        _configs = ConfigsProject()
        channels = listCSV().getList(_configs.get_groups_csv_path())
        _str = "📝 Lista de canais\n"
        for channel in channels:
            _str = _str + "📌 (http://t.me/{0})\n".format(channel[0])
        return _str

    def listUserPhone(self):
        """
        Metodo responsavel por lista todos os usuários que tem algum numero de telefone
        """
        _configs = ConfigsProject()
        phones = listCSV().getList(_configs.get_users_csv_path())
        _str = "📝 Lista de phones\n"
        cont = 0
        for phone in phones:
            if phone[2] != "None":
                _str = _str + "📱 {0}\n".format(phone[2])
                cont += 1
        _str = _str + "Total {0}\n".format(cont)
        return _str

    def listUserUsername(self):
        """
        Metodo responsavel por lista todos os usuários que tem algum username
        """
        _configs = ConfigsProject()
        users = listCSV().getList(_configs.get_users_csv_path())
        _str = "📝 Lista de users\n"
        cont = 0
        for user in users:
            if user[1] != "None":
                _str = _str + "📱 {0}\n".format(user[1])
                cont += 1
        _str = _str + "Total {0}\n".format(cont)
        return _str
