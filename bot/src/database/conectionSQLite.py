# conectionSQLite.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


import os
import sqlite3
from configs import ConfigsProject


class ConectionSQLite(object):
    def __init__(self):
        """
        Metodo contem variaveis reponsavel por interligar os metodos
        """
        self.connection = None
        self.c = None

    def createTable(self):
        """
        Method responsible for creating a database and tables
        """
        try:
            sql = """
            CREATE TABLE IF NOT EXISTS tbUser (
                id              INTEGER  	    NOT NULL PRIMARY KEY AUTOINCREMENT,
                idUser          INTEGER   	    NOT NULL,
                username        VARCHAR(255)    NOT NULL,
                phone           VARCHAR(255)    NOT NULL
            );
            """
            self.c.execute(sql)
            sql = """
            CREATE TABLE IF NOT EXISTS tbGroup (
                id              INTEGER         NOT NULL PRIMARY KEY AUTOINCREMENT,
                idGroup         INTEGER 	    NOT NULL,
                name		    VARCHAR(255)    NOT NULL
            );
            """
            self.c.execute(sql)
            sql = """
            CREATE TABLE IF NOT EXISTS tbUserLinkToGroup (
                id              INTEGER		    NOT NULL PRIMARY KEY AUTOINCREMENT,
                idUser	    	INTEGER		    NOT NULL,
                idGroup 	    INTEGER		    NOT NULL
            );
            """
            self.c.execute(sql)
        except Exception as e:
            print(e)

    def openConnection(self):
        """
        Method responsible for opening a connection to the database
        """
        try:
            configs = ConfigsProject()
            database_sqlite_path = configs.get_database_sqlite_path()
            self.connection = sqlite3.connect(database_sqlite_path)
            self.c = self.connection.cursor()
        except Exception as e:
            print(e)

    def closeConnection(self):
        """
        Method responsible for closing a connection with the database
        """
        try:
            self.connection.close()
        except Exception as e:
            print(e)

    def setInformation(self, sql=str(), val=()):
        """
        Metodo responsavel por gravar informações no banco de dados
        """
        try:
            self.c.execute(sql, val)
            self.connection.commit()
        except Exception as e:
            print(e)

    def getInformation(self, sql=str()):
        """
        Metodo responsavel por ler informações no banco de dados
        """
        _list = []
        try:
            for row in self.c.execute(sql):
                _list.append(row)
        except Exception as e:
            print(e)
        return _list
            
