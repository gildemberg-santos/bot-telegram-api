# dao.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


from src.database.conectionSQLite import ConectionSQLite

class Dao(object):
    def __init__(self):
        """
        Method responsible for instantiating the ConectionSQLite class
        """
        conn = ConectionSQLite()
        conn.openConnection()
        conn.createTable()
        conn.closeConnection()

    def getAllIdChatUser(self, values={}):
        """
        Method responsible for return a new user to the database
        """
        conn = ConectionSQLite()
        conn.openConnection()
        sql = "SELECT idUser FROM tbUser"
        val = ()
        data = conn.getInformation(sql=sql)
        conn.closeConnection()
        return data

    def addUser(self, values={}):
        """
        Method responsible for adding a new user to the database
        """
        conn = ConectionSQLite()
        conn.openConnection()
        sql = "INSERT INTO tbUser (idUser, username, phone) VALUES (?, ?, ?)"
        val = (values['idUser'], values['username'], values['phone'])
        conn.setInformation(sql=sql, val=val)
        conn.closeConnection()

    def toEditUser(self):
        """
        Method responsible for editing user information in the database
        """
        self.conn.openConnection()
        # Command
        self.conn.closeConnection()

    def deleteUser(self):
        """
        Method responsible for deleting users in the database
        """
        self.conn.openConnection()
        # Command
        self.conn.closeConnection()

    def getAllGroup(self, values={}):
        """
        Method responsible for return a new user to the database
        """
        conn = ConectionSQLite()
        conn.openConnection()
        sql = "SELECT idUser FROM tbUser"
        val = ()
        data = conn.getInformation(sql=sql)
        conn.closeConnection()
        return data

    def addGroup(self, values={}):
        # Tem que ajustar o SQL, não esta mandando os dados para a tabela certa
        """
        Method responsible for adding a new group to the database
        """
        self.conn.openConnection()
        conn.openConnection()
        sql = "INSERT INTO tbUser (idUser, username, phone) VALUES (?, ?, ?)"
        val = (values['idUser'], values['username'], values['phone'])
        conn.setInformation(sql=sql, val=val)
        self.conn.closeConnection()

    def toEditGroup(self):
        """
        Method responsible for editing group information in the database
        """
        self.conn.openConnection()
        # Command
        self.conn.closeConnection()

    def deleteGroup(self):
        """
        Method responsible for deleting group in the database
        """
        self.conn.openConnection()
        # Command
        self.conn.closeConnection()

    def addUserLinkToGroup(self):
        """
        Method responsible for adding a new link to the database
        """
        self.conn.openConnection()
        # Command
        self.conn.closeConnection()

    def toEditUserLinkToGroup(self):
        """
        Method responsible for editing link information in the database
        """
        self.conn.openConnection()
        # Command
        self.conn.closeConnection()

    def deleteUserLinkToGroup(self):
        """
        Method responsible for deleting link in the database
        """
        self.conn.openConnection()
        # Command
        self.conn.closeConnection()
