# listcsv.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


import csv


class listCSV:

    def __init__(self):
        """
        Metodo não tem nenhuma ação
        """
        pass

    def getList(self, path):
        """
        Metodo responsavel por ler um arquivo com a extenção CSV
        """
        _list = []
        try:
            with open(path, 'rt', encoding="utf8") as f:
                data = csv.reader(f)
                for row in data:
                    _list.append(row)
        except Exception as e:
            print(e)
        return _list

    def setList(self, path, _list):
        """
        Metodo responsavel por gravar informações em um arquivo CSV
        """
        try:
            with open(path, 'a') as f:
                write = csv.writer(f, delimiter=',', lineterminator='\n')
                for row in _list:
                    write.writerow(row)
        except Exception as e:
            print(e)

