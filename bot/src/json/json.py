# json.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


import os
import json
import requests


class Json(object):
    def __init__(self):
        """
        Metodo não tem nenhuma ação
        """
        pass

    def download(self, url='', method="GET", payload={}, querystring={}):
        """
        Metodo responsavel por baixar um arquivo
        """
        try:
            headers = {
                'accept-encoding': "gzip"
            }
            response = requests.request(
                method=method,
                url=url,
                data=payload,
                params=querystring,
                headers=headers)
            if response.status_code != 200:
                return None
            return response
        except Exception as e:
            return None

    def downloadJson(self, url='', method="GET", payload={}, querystring={}):
        """
        Metodo responsavel por baixar um arquivo JSON
        """
        dw = self.download(url=url, method=method, payload=payload, querystring=querystring)
        if not dw:
            print("Por favor verifique sua conecção com a internet!")
            return None
        dw = json.loads(dw.content)
        return dw

    def writejson(self, dados='', nome=''):
        """
        Metodo responsavel por gravar um arquivo JSON
        """
        try:
            with open("{0}{1}.{2}".format('data/', nome, 'json'), 'w') as fdw:
                json.dump(json.loads(dados), fdw, indent=4)
        except BaseException:
            pass

    def readjson(self, nome=''):
        """
        Metodo responsavel por ler uma arquivo JSON
        """
        dados = None
        try:
            with open("{0}{1}.{2}".format('data/', nome, 'json'), 'r') as fdr:
                dados = fdr.read()
        except BaseException:
            pass

        if not dados:
            return None
        return json.loads(dados)

