# configs.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


from src.json.json import Json


class ConfigsTelegram:
    def __init__(self):
        """
        Metodo responsavel por ler o arquivo JSON de configuração
        """
        self.data = None
        try:
            _json = Json()
            data = _json.readjson(nome='config')['telegram_session']
            self.data = data
            del(_json, data)
        except Exception as e:
            self.data = None
            print(e)

    def get_api_id(self):
        """
        Metodo responsavel por retornar o id da api
        """
        if not self.data:
            return None
        return int(self.data['api_id'])

    def get_api_hash(self):
        """
        Metodo responsavel por retornar a hash da api
        """
        if not self.data:
            return None
        return str(self.data['api_hash'])

    def get_number(self):
        """
        Metodo responsavel por retornar o número de telefone da api
        """
        if not self.data:
            return None
        return str(self.data['number'])

    def get_bot_token(self):
        """
        Metodo responsavel por retornar a token do bot
        """
        if not self.data:
            return None
        return str(self.data['bot_token'])

    def get_poxy(self):
        """
        Metodo responsavel por retornar o poxy
        """
        if not self.data:
            return None
        if not self.data['proxy']:
            return None
        return self.data['proxy']


class ConfigsProject:
    def __init__(self):
        """
        Metodo responsavel por ler o arquivo JSON de configuração
        """
        self.data = None
        try:
            _json = Json()
            data = _json.readjson(nome='config')['project_configuration']
            self.data = data
            del(_json, data)
        except Exception as e:
            self.data = None
            print(e)

    def get_groups_csv_path(self):
        if not self.data:
            return None
        return str(self.data['groups_csv_path'])

    def get_groups_json_path(self):
        if not self.data:
            return None
        return str(self.data['groups_json_path'])

    def get_users_csv_path(self):
        if not self.data:
            return None
        return str(self.data['users_csv_path'])

    def get_users_json_path(self):
        if not self.data:
            return None
        return str(self.data['users_json_path'])

    def get_database_sqlite_path(self):
        if not self.data:
            return None
        return str(self.data['database_sqlite_path'])

    def get_database_mongodb_path(self):
        if not self.data:
            return None
        return str(self.data['database_mongodb_path'])

    def get_api_url(self):
        if not self.data:
            return None
        return str(self.data['api_url'])

