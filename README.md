# Bot Telegram
bot-telegram-api
Incluso API com ligação entre os úsuarios, grupos e mensagens; Bot para spammar mensagens.

## Lista de Imprementações
1. Informar quem esta logado (Bot ou User).
2. Excerrar a execução da aplicação.
3. Menu para alterar entre Bot e User.
4. Entrada em novos grupos e canais pelo User.
5. Coletar dos Canais e Grupos Id, Username e Phone pelo User.

## Problemas da API
1. Intervalo de ação de 60 a 80 segundos (Canais e Grupos)
2. Quantidade de 20 a 50 usuários por dia (Canais e Grupos)
3. Não tem quantidade certa de envios por dia (Mensagem "Pode ser bloquado por execesso de denuncias")

## Como utilizar
* Como obter API ID: https://core.telegram.org/api/obtaining_api_id
* Criação do MongoDB Atlas: https://www.mongodb.com/cloud/atlas
* Necessário criar arquivos de configurações.
* Arquivo de configuração do bot na pasta /bot/configs.py

#####Python
```Python
    userLogin = {
        'api_id': 1111111,
        'api_hash': 'xxxxxxxxxxxxxxxxxxxxxxx',
        'number': '+11111111111',
        'proxy': None
    },
    projeto = {
        'groups_path': 'data/channels.csv',
        'users_path': 'data/users.csv',
        'API_url': 'http://localhost:8080'
    }
```

Criação do arquivo channels.csv para adicionar os grupos na pasta /bot/data/channels.csv 
Padrão Nome, Hash do grupo e tipo opcional.
#####CSV
```CSV
    ProgramadoresBrasil
    comunidadefedorabrasil
    fedorabr
    deepin
```

Criação do arquivo users.csv para adicionar os usuários na pasta /data/users.csv
Padrão ID, Username e Phone.
#####CSV
```CSV
    290623448,CadsObjectPascal,None
    958977349,felipeamodio,None
    994851877,lucasantonioofc,None
    803208485,andreiwisley,None
```

Arquivo de configuração da API na pasta /api/src/config/default.json
#####JSON
```Json
    {
        "database": {
            "link": "linkdoBD"
        },
        "server": {
            "port": 8080
        }
    }
```

## Bibliotecas necessárias python
1. asyncio
2. requests
3. telethon

Documentação do telethon: <https://docs.telethon.dev/en/latest/>

###Instalar bibliotecas node
```Shell
    cd pasta/API && npm install
```

###Rodar
Executar o servidor :
```Shell
    cd pasta/API/src/ && node server.js
```
Executar o bot: 
```Shell
    cd pasta/bot && python bot.py
```

## Material para estudo 
Parei na aula 07 para a criação da API 
